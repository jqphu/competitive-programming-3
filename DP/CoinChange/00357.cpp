#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;


int main(void)
{
    int n;
    int coins[] = {1, 5, 10, 25, 50};
    // The value as well as the TYPE!
    unsigned long long dp[30001][5];
    memset(dp, 0, sizeof(dp));
    for(int i = 0; i < 5; ++i)
    {
        dp[0][i] = 1; 
    }

    for(int i = 1; i < 30000 + 1; ++i)
    {
        for(int j = 0; j < 5; ++j)
        {
            if( i < coins[j])
            {
                dp[i][j] = dp[i][j-1];
                continue;
            }
            if(j == 0)
            {
                dp[i][j] = 1; 
            }
            else
            {
                dp[i][j] = dp[i][j-1] + dp[i-coins[j]][j];
            }

        }
    }

    while(cin >> n)
    {
        if(dp[n][4] == 1)
        {
            printf("There is only %llu way to produce %d cents change.\n", dp[n][4], n);
        }
        else
        {

            printf("There are %llu ways to produce %d cents change.\n", dp[n][4], n);
        }
    }
}
