#include <cstdio>
#include <iostream>
#include <cstring>

using namespace std;

int dp[15200];

int main(void)
{
    int t;
    cin >> t;
    

    while(t--)
    {
        memset(dp, 0, sizeof(dp)); 
        int a, b;
        cin >> a >> b;
        dp[0] = a;
        dp[1] = b;

        for(int i = 2; i < 15200; ++i)
        {
            dp[i] = (dp[i-1] + dp[i-2])%10000;
        }

        unsigned long long n, m;
        cin >> n >> m;
        if( m == 1 ) 
        {
            n = n % 60;
            cout << dp[n] % 10 << endl;
        }
        else if ( m == 2)
        {
            n = n % 300;
            cout << dp[n] % 100 << endl;
        }
        else if ( m == 3)
        {
            n = n % 1500;
            cout << dp[n] % 1000 << endl;
        }
        else if ( m == 4)
        {
            n = n % 15000;
            cout << dp[n] % 10000 << endl;
        }



       
        
    }

}
