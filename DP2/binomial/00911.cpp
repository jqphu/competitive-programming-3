#include <cstdio>
#include <iostream>

using namespace std;


int cat(int n)
{
    if( n == 0) return 1;
    

    return cat(n-1)*(2*n)*(2*n-1)/((n+1)*(n));
}

int main(void)
{
    int n;
    int i = 0;
    while(cin >> n)
    {
        if(i) cout << endl;
        i++;

        cout << cat(n)<< endl;
    }
}
