#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int k;

unsigned long long dp[51][51];
int main(void)
{
    int T;
    cin >> T;

    memset(dp, 0, sizeof(dp));
    for(int i = 0; i < 50 + 1; ++i)
    {
        dp[i][0] = 1;
        dp[i][i] = 1;
    }

    for(int i = 1; i < 50 + 1; ++i)
    {
        for(int j = 1; j < 50 + 1; ++j)
        {
            if( j > i ) continue;
            dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
        }
    }

    for(int i = 0; i < T; ++i)
    {
        printf("Case %d: ",i+1);
        char X[120];
        char Y[120];

        scanf("%*c%*c%101[^+]%*c%101[^)]%*c%*c%d",X,Y,&k);

        for(int i = 0; i < k + 1; ++i)
        {
            unsigned long long coeff = dp[k][i];
            if(coeff != 0 && coeff != 1)
            {
                cout << coeff;
                cout << '*';
            }

            if(i != k)
            {
                printf("%s", X);
                if(k - i != 1)
                {

                    printf("^%d", k - i );

                }
                if(i != 0)
                {

                    cout << "*";
                }

            }

            if(i != 0)
            {
                printf("%s", Y);
                if(i != 1)
                {

                    printf("^%d", i );
                }

            }
            if(i != k)
            {
                cout << "+";
            }
        }
        cout << endl;
    }





}
